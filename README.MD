# Asynchronous Modules for JavaScript

This is kind of a workaround until top level await hits all the major browsers.

---

## AsyM module usage

```js
/**
 * AsyM constructor
 * ----------------------------------------------------------
 * genarator parameter
 * type: function
 * ----------------------------------------------------------
 * create a new AsyM module that can be automatically
 * resolved by the import function
 */
export default new AsyM(async () => {
    // your module code
});

/**
 * AsyM import method
 * ----------------------------------------------------------
 * url parameter
 * type: string | Array(string)
 * ----------------------------------------------------------
 * imports modules and resolves AsyM's, extracts names
 * into destructurable elements
 * allows multiple imports at the same time
 */
const [default, {member1, member2, …}] = await AsyM.import("module-name");
// or
const [
    [default1, {member1_1, member1_2, …}],
    [default2, {member2_1, member2_2, …}]
] = await AsyM.import([
    "module-name1",
    "module-name2"
]);

/**
 * AsyM importDyn method
 * ----------------------------------------------------------
 * url parameter
 * type: string
 * ----------------------------------------------------------
 * imports modules and resolves AsyM's
 * 
 * in-place-function for dynamic imports
 */
const {default: default1, member1, member2} = await AsyM.importDyn("module-name");
// or
AsyM.importDyn("module-name").then(({default: default1, member1, member2}) => {
    // do stuff
});
```

---

## gulp plugin usage

the following examples show how to use the plugin part to auto-augment javascript files

```js
const gulpAsyM = require("asym/GulpAsyM");

function copyScript() {
    return gulp.src(`/script/**/*.js`)
        .pipe(gulpAsyM())
        .pipe(gulp.dest(`/output`));

/**
 *  path parameter
 * ----------------------------------------------------------
 * default: "asym/src"
 * type: string
 * ----------------------------------------------------------
 * set the path where the AsyM modules will be located
 * 
 * ! IMPORTANT !
 * move the AsyM modules to the defined path
 */
function copyScriptAsyMPath() {
    return gulp.src(`/script/**/*.js`)
        .pipe(gulpAsyM({path: "path_to_asym_module_root"}))
        .pipe(gulp.dest(`/output`));
}

/**
 * optional alike parameter
 * ----------------------------------------------------------
 * default: null
 * type: string | RegExp
 * ----------------------------------------------------------
 * assumes that "importModule" works like AsyM's import and
 * can therefore be replaced directly
 */
function copyScriptAsyMPath() {
    return gulp.src(`/script/**/*.js`)
        .pipe(gulpAsyM({alike: "importModule"}))
        .pipe(gulp.dest(`/output`));
}
// or as regex
function copyScriptAsyMPath() {
    return gulp.src(`/script/**/*.js`)
        .pipe(gulpAsyM({alike: /importModule/}))
        .pipe(gulp.dest(`/output`));
}
```

---

### escaping imports

```js
/* asym-import: on */
// asym-import: on
/* asym-import: off */
// asym-import: off
```

disables/enables the asym import rewrite

---

### supported syntax

```js
/* import */
import defaultMember from "module-name";
import defaultMember, { member [ , […] ] } from "module-name";
import defaultMember, * as alias from "module-name";
import { member as alias } from "module-name";
import { member } from "module-name";
import { member1 , member2 as alias2 [ , […] ] } from "module-name";
import * as name from "module-name";
import "module-name";

/* dynamic import */
import("module-name");
await import("module-name");
const module = await import("module-name");
import("module-name").then(…).catch(…);

/* export */
export var name1 = value;
export let name1 = value;
export const name1 = value;
export function (…) { … }
export async function (…) { … }
export function* (…) { … }
export async function* (…) { … }
export default expression;

---

add documentation on AsyM itself