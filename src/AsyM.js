const GENERATOR = new WeakMap();

async function resolveModule(module) {
    try {
        if (module.default instanceof AsyM) {
            return await GENERATOR.get(module.default) ?? {};
        }
        return module ?? {};
    } catch(err) {
        throw new Error(`Error resolving module\n${err}`);
    }
}

async function extractModule(module) {
    try {
        const {default: def, ...other} = module;
        return [def, other];
    } catch(err) {
        throw new Error(`Error extracting module\n${err}`);
    }
}

async function importAsyM(url) {
    try {
        const module = await import(url);
        return await resolveModule(module);
    } catch(err) {
        throw new Error(`Error appending module "${new URL(url, location.origin)}"\n${err}`);
    }
}

async function importExtractedAsyM(url) {
    try {
        const module = await import(url);
        const resolved = await resolveModule(module);
        return await extractModule(resolved);
    } catch(err) {
        throw new Error(`Error appending module "${new URL(url, location.origin)}"\n${err}`);
    }
}

async function importInternal(generator, urls) {
    const res = [];
    for (const i of urls) {
        res.push(importExtractedAsyM(i));
    }
    const reqs = await Promise.all(res);
    return await generator(reqs);
}

export default class AsyM {

    constructor(urls, generator) {
        if (!Array.isArray(urls)) {
            throw new TypeError("requirements must be an array of urls");
        }
        if (typeof generator != "function") {
            throw new TypeError(`generator must be of type "function" but is "${typeof generator}"`);
        }
        GENERATOR.set(this, importInternal(generator, urls));
    }

    static async import(url) {
        if (Array.isArray(url)) {
            const res = [];
            for (const i of url) {
                res.push(importExtractedAsyM(i));
            }
            return await Promise.all(res);
        } else {
            return await importExtractedAsyM(url);
        }
    }

    static async importDyn(url) {
        return await importAsyM(url, false);
    }

}
