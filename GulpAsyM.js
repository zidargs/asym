const { Transform } = require("stream");
const { augmentFile } = require("./Augmentor.js");

module.exports = function(params) {
    const transformStream = new Transform({ objectMode: true });
    transformStream._transform = function(file, encoding, callback) {
        const contents = augmentFile(String(file.contents), params);
        if (file.isBuffer() === true) {
            file.contents = Buffer.from(contents);
        } else {
            file.contents = contents;
        }
        callback(null, file);
    };
    return transformStream;
};
