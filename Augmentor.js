const exportsName = "$__exports__$";
const asymName = "$__AsyM__$";
const pathName = "$__Path__$";

const LNBR_SEQ = /(?:\r\n|\n|\r)/g;
const IMPORT_SWITCH_SEQ = /(?:\/\*|\/\/).*asym-import:\s*(on|off)/g;

const r_import_export_wildcard = /\*\s*as\s+([a-zA-Z0-9_$]+)/;
const r_import_export_as = /([a-zA-Z0-9_$]+) as ([a-zA-Z0-9_$]+)/g;

const r_assigned_import = /import\s+((?:[a-zA-Z0-9_$]+)?)\s*,?\s*((?:\{[^{}]*\}|\*\s*as\s+[a-zA-Z0-9_$]+)?)\s+from\s*(".{0,2}\/[^"]+")/;
const r_unassigned_import = /import\s*(".{0,2}\/[^"]+")/;
const r_not_import = /\.import\s*\(/g;
const r_dynamic_import = /import\s*\(/g;

const r_class_export = /export\s+(default\s+)?(class\s+([a-zA-Z0-9_$]+).*)/;
const r_value_export = /export\s+(default\s+)?((?:const|let|var)\s+([a-zA-Z0-9_$]+)\s*=.*)/;
const r_function_export = /export\s+(default\s+)?((?:async\s+)?function\*?\s*([a-zA-Z0-9_$]+).*)/;
const r_default_export = /export\s+default\s+(.*)/;
const r_named_export = /export\s+([a-zA-Z0-9_$]+)(.*)/;

exports.augmentFile = function augmentFile(fileContent, {path: asymPath, alike}) {
    if (typeof asymPath != "string") {
        asymPath = "asym/src";
    }
    if (typeof alike != "string" && !(alike instanceof RegExp)) {
        alike = null;
    }

    let is_import_active = true;
    const assigned_imports = [];
    const unassigned_imports = [];
    const untouched_imports = [];
    const export_values = {};
    const script = fileContent.split(LNBR_SEQ).map(string => {
        // check switch sequence
        const rImportSwitch = IMPORT_SWITCH_SEQ.exec(string);
        if (rImportSwitch != null) {
            is_import_active = rImportSwitch[1] == "on";
            return;
        }
        // import assigned
        const resAssignedImport = r_assigned_import.exec(string);
        if (resAssignedImport != null) {
            if (is_import_active) {
                let names = resAssignedImport[2];
                let url = resAssignedImport[3];
                if (names != "") {
                    if (names.startsWith("*")) {
                        names = names.replace(r_import_export_wildcard, "$1");
                    } else {
                        names = `, ${names.replace(r_import_export_as, "$1: $2")}`;
                    }
                }
                if (url.startsWith("\".")) {
                    url = `path.getAbsolute(${url})`
                }
                assigned_imports.push([`[${resAssignedImport[1]}${names}]`, `${url}`]);
            } else {
                untouched_imports.push(resAssignedImport[0]);
            }
            return;
        }
        // import unassigned
        const resUnassignedImport = r_unassigned_import.exec(string);
        if (resUnassignedImport != null) {
            if (is_import_active) {
                let url = resUnassignedImport[1];
                if (url.startsWith("\".")) {
                    url = `path.getAbsolute(${url})`
                }
                unassigned_imports.push(url);
            } else {
                untouched_imports.push(resUnassignedImport[0]);
            }
            return;
        }
        // class exports
        const resClassExport = r_class_export.exec(string);
        if (resClassExport != null) {
            const name = resClassExport[3];
            const def = resClassExport[1] != null && !!resClassExport[1].length;
            if (def) {
                export_values["default"] = name;
            } else {
                export_values[name] = name;
            }
            return resClassExport[2];
        }
        // value exports
        const resValueExport = r_value_export.exec(string);
        if (resValueExport != null) {
            const name = resValueExport[3];
            const def = resValueExport[1] != null && !!resValueExport[1].length;
            if (def) {
                export_values["default"] = name;
            } else {
                export_values[name] = name;
            }
            return resValueExport[2];
        }
        // function exports
        const resFunctionExport = r_function_export.exec(string);
        if (resFunctionExport != null) {
            const name = resFunctionExport[3];
            const def = resFunctionExport[1] != null && !!resFunctionExport[1].length;
            if (def) {
                export_values["default"] = name;
            } else {
                export_values[name] = name;
            }
            return resFunctionExport[2];
        }
        // import dynamic
        const resNotImport = r_not_import.exec(string);
        if (resNotImport == null) {
            string = string.replace(r_dynamic_import, `${asymName}.importDyn(`);
        }
        // exports
        string = string.replace(r_default_export, (match, p1) => `${exportsName}.default=${p1}`);
        string = string.replace(r_named_export, (match, p1, p2) => `${exportsName}.${p1}${p2}`);
        // alike
        if (alike != null) {
            string = string.replace(alike, `${asymName}.import`);
        }
        return string;
    }).filter(e => e != null && e.trim() != "").join("\n");

    // --- import ---
    const import_names = [];
    const import_urls = [];
    if (assigned_imports.length) {
        for (const [name, url] of assigned_imports) {
            import_names.push(`\t${name}`);
            import_urls.push(`\t${url}`);
        }
    }
    if (unassigned_imports.length) {
        for (const url of unassigned_imports) {
            import_urls.push(`\t${url}`);
        }
    }
    let importURLString = "[]";
    let importNameString = "";
    if (import_urls.length) {
        importURLString = `[\n${import_urls.join(",\n")}\n]`;
        if (import_names.length) {
            importNameString += `[\n${import_names.join(",\n")}\n]`;
        }
    }
    let untouched_import_string = "";
    if (untouched_imports.length) {
        untouched_import_string += "\n/* ASYM: START UNTOUCHED IMPORTS ----------------------------------------- */\n";
        untouched_import_string += untouched_imports.join("\n");
        untouched_import_string += "\n/* ASYM: END UNTOUCHED IMPORTS ------------------------------------------- */";
    }

    // --- export ---
    let exportString = "";
    for (const [name, value] of Object.entries(export_values)) {
        exportString += `\n\t\t${exportsName}.${name} = ${value};`;
    }

    // --- output ---
    return `
/**
 * code modified by AsyM
 */
import ${pathName} from "${asymPath}/Path.js";
import ${asymName} from "${asymPath}/AsyM.js";${untouched_import_string}
const path = new ${pathName}(import.meta.url);
export default new ${asymName}(${importURLString}, async (${importNameString}) => {
    const ${exportsName} = {};
    {
/* ASYM: START SCRIPT ---------------------------------------------------- */
${script}
/* ASYM: END SCRIPT ------------------------------------------------------ */${exportString}
    }
    return ${exportsName};
});`;
}
